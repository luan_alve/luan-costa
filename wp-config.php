<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'wordpress');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%`DcbNK/I-l-59!msY[m:#R-^7O$`}1_<7.=BDl117;,Od#EbA8qi)6>+o20k{=E');
define('SECURE_AUTH_KEY',  '63LTeV/R+4nJ7jL3|nWqX$ML[II$|xfxE6``S4W46.4$^RR||n{i+~@V4p?s|K9,');
define('LOGGED_IN_KEY',    '!]2IUDiMG=4zFq)S_E|Y8c^k8O >Dq:yU}d|Si/10O-k(ebFJCn]fYD^>YGjIOl%');
define('NONCE_KEY',        'k=$Q,;Zzu/TP@IVD3I>lp(/jNU}xO+d1)u6,2<rs5*XlP< jvxJ`-p-TcG8g7P9T');
define('AUTH_SALT',        'Du&fn6`fH%&LKdJG?9-Qm80QC0!x*c6HNTBksq%kWT5&MlI/!jN_N!0VKID/f#U6');
define('SECURE_AUTH_SALT', 'x%]#/KySQ}vf>mhrn!Z9_Sb17N>~[jZ>zdT4YtB!AH:0+XXYk}eA{bw.o6 _;A^,');
define('LOGGED_IN_SALT',   'rakV_g{G+URn^`uDo2);LMb OJqz8sRcNM0>_Tya+wg0/+<*`gua>wmvQ+1j#T B');
define('NONCE_SALT',       'Rs^@WP-|4/oAY,[HU7<Eb$mbRKA9cZ^?oTq.+%(iLNhcH+D:H/= dawUXQxdOG|w');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');

